import os
import sys

TEST_BLOCK = {}


def setup():
    """
    **ENSO_PYVER** - *The system's Python version.*

    **ENSO_EDITOR_TITLE** - *The system's Python version.*

    **ENSO_VIEWER_TITLE** - *The system's Python version.*

    **ENSO_FRAMEWORK_ROOT** - *The framwork's root directory.*

    **ENSO_ROOT** - *Haw-dini's root directory.*

    **ENSO_MOD_PATH** - *Root directory for modules.*

    **ENSO_LOG_PATH** - *Root directory for logging.*

    **ENSO_DEFAULT_JSON** - *Full path of the default JSON graph.*

    **ENSO_DEFAULT_ICON** - *Full path of the default node icon.*

    **ENSO_ICONS_PATH** - *Root directory for the icons.*

    **ENSO_GLOBAL_CSS** - *Default CSS style for the UI.*

    **ENSO_LANGUAGE** - *language name.*

    **TEST_BLOCK** - *default datablock for testing.*

    **ENSO_LOG** - *Default handler name.*

    >>> setup()

    """
    global TEST_BLOCK

    global_css = "QDialog {background-color: dimgrey} "
    global_css+="QLineEdit {background-color: slate; color: silver; border: none} "
    global_css+="QToolButton {background-color: dimgrey; border: none} "
    global_css+="QToolButton::hover {background-color: slategrey; border: none} "
    global_css+="QMenuBar {border: none} "
    global_css+="QTreeWidget {background-color: #505050; color: silver; border: none} "
    global_css+="QTreeWidget::item:hover {background-color:slategrey;} "
    global_css+="QHeaderView::section {background-color: dimgrey; border: none} "
    global_css+="QGroupBox::indicator:unchecked {image: url(..\\ui\\icons\\group_collapse_close.png);} "
    global_css+="QGroupBox::indicator:checked {image: url((..\\ui\\icons\\group_collapse_open.png);} "

    fw_root = os.path.dirname(os.path.dirname(__file__))
    hw_root = os.path.dirname(fw_root)
    icons_root = os.path.join(fw_root,"ui","icons")
    title_template = "Haw-dini {} for {} (Alpha, Python-{})"
    lang = "py"
    pyver = str(sys.version_info[0])
    test_json = os.path.join(hw_root,"graphs","tester_{}.json".format(lang))

    envs = [
            "ENSO_FRAMEWORK_ROOT",
            "ENSO_ROOT",
            "ENSO_MOD_PATH",
            "ENSO_LOG_PATH",
            "ENSO_ICONS_PATH",
            "ENSO_DEFAULT_ICON",
            "ENSO_DEFAULT_JSON",
            "ENSO_GLOBAL_CSS",
            "ENSO_LANGUAGE",
            "ENSO_STRICT",
            "ENSO_PROPAGATE",
            "ENSO_PYVER",
            "ENSO_LOG",
            "ENSO_EDITOR_TITLE",
            "ENSO_VIEWER_TITLE",
            ]

    nulls = [
             fw_root, # ENSO_FRAMEWORK_ROOT
             hw_root, # ENSO_ROOT
             os.path.abspath(os.path.join(hw_root,"modules",lang)), # ENSO_MOD_PATH
             os.path.abspath(os.path.join(hw_root,"log")), # ENSO_LOG_PATH
             icons_root, # ENSO_ICONS_PATH
             os.path.join(icons_root,"node.png"), # ENSO_DEFAULT_ICON
             test_json, # ENSO_DEFAULT_JSON
             global_css, # ENSO_GLOBAL_CSS
             lang, # ENSO_LANGUAGE
             "1", # ENSO_STRICT
             "1", # ENSO_PROPAGATE
             pyver, # ENSO_PYVER
             "enso", # ENSO_LOG
             title_template.format("Editor",lang,pyver), # ENSO_EDITOR_TITLE
             title_template.format("Viewer",lang,pyver), # ENSO_VIEWER_TITLE
             ]

    for e in envs:
        if not os.environ.get(e,None):
            os.environ[e] = nulls[envs.index(e)]

    # might be better at config or enso's init
    TEST_BLOCK.update({"what" : "This is Hawdini",
                       "where" : "Made in Japan",
                       "when" : "On my spare time",
                       #"why" : "To make a better world",
                       "who" : "Richard Haw"})

    print("Configs initiated...")