from __future__ import print_function

from PySide2.QtWidgets import (QInputDialog,
                               QLineEdit)


class Spawn(QInputDialog):
    """
    Creates a modal dialog with text field.

    **parameters**, **types**, **return** and **return types**

    :param show_text: Title label.
    :type show_text: str

    :param def_txt: Initial text.
    :type def_txt: str
    """

    def __init__(self,show_text="Enter text",def_txt=""):
        super(Spawn,self).__init__()

        self.show_text = show_text
        self.def_txt = def_txt

        self.text,self.ok = self.getText(self,
                                         "Haw-dini Dialog",
                                         "{}:".format(self.show_text),
                                         QLineEdit.Normal,
                                         self.def_txt)


    def out(self):
        """
        Return whatever is in the text field.

        :return: Returns the text value.
        :rtype: None or string
        """

        if self.text and self.ok:
            return str(self.text)
        else:
            return