from __future__ import print_function

from PySide2.QtWidgets import (QGroupBox)
from PySide2.QtCore import Qt


class Spawn(QGroupBox):
    def __str__(self):
        return __name__


    def __init__(self,title_text,label_height=20):
        super(Spawn,self).__init__()

        self.title_text = title_text

        self._title_formatted = ""
        self._label_height = label_height
        self._orig_height = self.height()

        self.newTitle(self.title_text)
        self.setFlat(True)
        self.setCheckable(True)
        self.setAlignment(Qt.AlignAbsolute)
        self.toggled.connect(self._resizer)


    def _resizer(self):
        _win = self.window()
        _win_width = _win.size().width()
        _win_height = _win.size().height()
        _new_height = 0

        if self.isChecked():
            self.setMaximumHeight(self._orig_height)
            _new_height = _win_height+(self._orig_height-self._label_height)
            self.newTitle(self.title_text)
        else:
            self._orig_height = self.height()
            self.setMaximumHeight(self._label_height)
            self.newTitle(self.title_text + " (collapsed)")
            _new_height = _win_height-(self._orig_height+self._label_height)

        _win.resize(_win_width,_new_height)


    def setLabelHeight(self,new_hgt):
        self._label_height = new_hgt
        self._resizer()


    def newTitle(self,new_title):
        self._title_formatted = "{} ".format(new_title)
        self.setTitle(self._title_formatted)