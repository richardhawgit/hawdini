from __future__ import division
from __future__ import print_function
# from __future__ import absolute_import

import uuid

from PySide2.QtCore import (Qt,
                            QRect)

from PySide2.QtGui import (QColor,
                           QBrush,
                           QPainter)

from PySide2.QtWidgets import QGraphicsScene

from framework.ui.widgets import (itemNode,
                                  startNode,
                                  groupNode)


class Spawn(QGraphicsScene):
    """
    Creates an instance of QGraphicsScene with reimplemented functions.
    Takes no arguements.

    **parameters**, **types**, **return** and **return types**

    :return: If strict is set to True, recursion will stop on Error.
    :rtype: None

    - Example::

        graphicsScene.Spawn()
    """

    def __init__(self,mode="editor"):
        super(Spawn,self).__init__()

        _grid_color = QColor(25,25,25,150)
        self.setBackgroundBrush(QBrush(_grid_color,
                                       Qt.CrossPattern))
        self.setMode(mode)


    def setMode(self,mode):
        _modes = ["editor","player","viewer"]
        if mode not in _modes:
            print("Valid modes: {}".format((",".join(_modes))))
            raise ValueError("Invalid mode: {}".format(mode))

        self.editable = mode == "editor"
        self.mode = mode;


    def keyPressEvent(self,event):
        """
        :meta private:
        """

        if not self.editable:
            return

        # delete selected
        if event.key() in (Qt.Key_Delete,Qt.Key_Backspace):
            for _w in self.selectedItems():
                if isinstance(_w,itemNode.Spawn):
                    for _out_w in _w.plug_out.out_wires:
                        _w.scene.removeItem(_out_w)

                    _in_wire =_w.plug_in.in_wire
                    if _in_wire:
                        _in_wire.removeFromSocket(_w.scene,
                                                  _in_wire,
                                                  _in_wire.target)
                    _w.scene.removeItem(_w)
                elif isinstance(_w,groupNode.Spawn):
                    _w.unparentChildren()
                    _w.scene.removeItem(_w)
                    self.clearSelection()

                    for _it in _w.group_widgets:
                        _it.setSelected(True)


    def resetToStarter(self):
        """
        Clears the scene.
        Creates a new starter node in the scene.

        :return: Pointer of the new starter node.
        :rtype: object
        """

        if not self.editable:
            return

        self.clear()
        _starter = startNode.Spawn("Start",
                                   self.sceneRect().width()/2,
                                   self.sceneRect().height()/2,
                                   self)
        _starter.uuid = uuid.uuid1()
        return _starter
