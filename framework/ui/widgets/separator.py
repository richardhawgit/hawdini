from PySide2.QtWidgets import QFrame


class Spawn(QFrame):
    def __str__(self):
        return __name__


    def __init__(self,horizontal,parent_layout=None):
        super(Spawn,self).__init__()

        if horizontal:
            self.setFrameShape(QFrame.HLine)
        else:
            self.setFrameShape(QFrame.VLine)

        self.setFrameShadow(QFrame.Plain)

        if parent_layout:
            parent_layout.addWidget(self)