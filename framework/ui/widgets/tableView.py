from PySide2.QtWidgets import (QTableView,
                               QHeaderView)
from PySide2.QtCore import Qt


class Spawn(QTableView):
    def __init__(self,model,sort_items=False):
        super(Spawn,self).__init__()

        self.model = model
        self.sort_items = False
        self._setup()


    def _setup(self):
        self.setModel(self.model)
        self.resizeColumnsToContents()
        self.setSortingEnabled(self.sort_items)
        self.setGridStyle(Qt.SolidLine)
        self.setAlternatingRowColors(True)
        self.verticalHeader().hide()
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.horizontalHeader().setDefaultAlignment(Qt.AlignLeft)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        # self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
