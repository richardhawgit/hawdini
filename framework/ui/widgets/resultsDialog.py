import os

from PySide2.QtWidgets import (QLabel,
                               QDialog,
                               QGroupBox,
                               QLineEdit,
                               QSizePolicy,
                               QHeaderView,
                               QHBoxLayout,
                               QVBoxLayout,
                               QPushButton,
                               QSpacerItem,
                               QPlainTextEdit)
from PySide2.QtCore import Qt

import tableModel,tableView,collapseGroup


class Spawn(QDialog):
    def __str__(self):
        return __name__


    def __init__(self,node_name,status,desc="none",msg="",errors_list=[]):
        super(Spawn,self).__init__()

        self.node_name = node_name
        self.errors_list = self._processErrors(errors_list)
        self.desc = ""
        self.message = ""
        self.status = status
        self.headers = ["Item","Type","Reason"]

        self.setModal(True)
        self._setup()
        self.setMessage(msg)
        self.setDesc(desc)
        self.setStyleSheet(os.getenv("ENSO_GLOBAL_CSS"))
        self.resize(600,800)
        self.exec_()


    def _setup(self):
        self.setWindowTitle("{}'s details".format(self.node_name))
        _main_layout = QVBoxLayout()
        _main_layout.setAlignment(Qt.AlignTop)
        self.setLayout(_main_layout)

        # information
        _info_box = collapseGroup.Spawn("Information:")
        _info_box.setCheckable(False)
        _info_box.setSizePolicy(QSizePolicy.Expanding,QSizePolicy.Fixed)
        _main_layout.addWidget(_info_box)

        _spec_layout = QVBoxLayout()
        _spec_layout.setAlignment(Qt.AlignTop)
        _info_box.setLayout(_spec_layout)

        # name
        _name_layout = QHBoxLayout()
        _spec_layout.addLayout(_name_layout)

        _name_lbl = QLabel("Node Name:")
        _name_lbl.setFixedWidth(85)
        _name_layout.addWidget(_name_lbl)

        self.name_txt = QLineEdit(self.node_name)
        self.name_txt.setToolTip("Change node name?")
        self.name_txt.setMinimumWidth(100)
        _name_layout.addWidget(self.name_txt)

        # source
        _module_layout = QHBoxLayout()
        _spec_layout.addLayout(_module_layout)

        _module_lbl = QLabel("Module Name:")
        _module_lbl.setFixedWidth(85)
        _module_layout.addWidget(_module_lbl)

        _module_txt = QLineEdit(self.node_name+".py")
        _module_txt.setToolTip("Source absolute URI")
        _module_txt.setMinimumWidth(100)
        _module_layout.addWidget(_module_txt)

        # status
        _status_layout = QHBoxLayout()
        _spec_layout.addLayout(_status_layout)

        _status_lbl = QLabel("Node Status:")
        _status_lbl.setFixedWidth(85)
        _status_layout.addWidget(_status_lbl)

        self.status_txt = QLineEdit(self.status)
        self.status_txt.setToolTip("Current node status.")
        self.status_txt.setMinimumWidth(100)
        self.status_txt.setEnabled(False)
        _status_layout.addWidget(self.status_txt)

        _spacer1 = QSpacerItem(10,0,vData=QSizePolicy.Minimum)
        _spec_layout.addItem(_spacer1)

        # description
        _desc_box = collapseGroup.Spawn("Description:")
        _desc_box.setChecked(False)
        _main_layout.addWidget(_desc_box)

        self._desc_txt = QPlainTextEdit(self)
        self._desc_txt.setEnabled(False)
        self._desc_txt.setMinimumWidth(150)

        _desc_layout = QVBoxLayout()
        _desc_layout.addWidget(self._desc_txt)
        _desc_box.setLayout(_desc_layout)

        # messages
        _memo_box = collapseGroup.Spawn("Messages:")
        _main_layout.addWidget(_memo_box)

        self._message_txt = QPlainTextEdit(self)
        self._message_txt.setEnabled(False)
        self._message_txt.setMinimumWidth(150)

        _memo_layout = QVBoxLayout()
        _memo_layout.addWidget(self._message_txt)
        _memo_box.setLayout(_memo_layout)

        # table
        _table_box = collapseGroup.Spawn("Errors:")
        _main_layout.addWidget(_table_box)

        self.tableModel = tableModel.Spawn(self,
                                             self.errors_list,
                                             self.headers)

        self.tableView = tableView.Spawn(self.tableModel)
        self.tableView.setMaximumHeight(300)
        _hori_head = self.tableView.horizontalHeader()
        _hori_head.setSectionResizeMode(0,QHeaderView.Fixed)
        _hori_head.setSectionResizeMode(1,QHeaderView.Fixed)
        _hori_head.setSectionResizeMode(2,QHeaderView.Stretch)

        _table_layout = QVBoxLayout()
        _table_layout.addWidget(self.tableView)
        _table_box.setLayout(_table_layout)

        # button
        _button_box = collapseGroup.Spawn("")
        _button_box.setFixedHeight(65)
        _button_box.setCheckable(False)
        _main_layout.addWidget(_button_box)

        _spacer2 = QSpacerItem(10,0,vData=QSizePolicy.Minimum)
        _spacer3 = QSpacerItem(10,0,vData=QSizePolicy.Minimum)

        self.button = QPushButton("Accept")
        self.button.clicked.connect(self.dummy)
        _main_layout.addWidget(self.button)

        _button_layout = QHBoxLayout()
        _button_layout.addItem(_spacer2)
        _button_layout.addWidget(self.button)
        _button_layout.addItem(_spacer3)
        _button_box.setLayout(_button_layout)


    def dummy(self):
        self.resize(self.size().width(),0)


    def setMemo(self,text_lines):
        for t in text_lines:
            self._message_txt.insertPlainText(t)


    def setDesc(self,strings):
        self.desc = strings
        self._desc_txt.setPlainText(self.desc)


    def setMessage(self,msg_str):
        self.message = msg_str
        self._message_txt.setPlainText(self.message)


    def _processErrors(self,raw_errors):
        safe_errors = list()

        if len(raw_errors) < 1:
            return [("","","")]

        for e in raw_errors:
            _a = e.get("item",None)
            _b = e.get("type","")
            _c = e.get("reason","")

            if _a is None:
                raise KeyError("Item not specified in error")

            safe_errors.append((str(_a),
                                str(_b),
                                str(_c))
                               )

        return safe_errors


if __name__ == "__main__":
    import sys
    from PySide2.QtWidgets import QApplication
    top_app = QApplication(sys.argv)

    errs = [("babalu_layer_longName","LayerItem","Not found"),
            ("baba_comp","CompItem","Incomplete items, missing layers"),
            ("baba_foot","FootageItem","File not found"),
            ("baba_folder","FolderItem","Japanese name, should be ASCII only"),
            ]

    sp = Spawn("babalu","my description",errs)
    sys.exit(0)