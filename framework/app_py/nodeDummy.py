from __future__ import print_function


class Spawn(object):
    """
    Creates a dummy object for app_py.main to run with.

    **parameters**, **types**, **return** and **return types**

    :param obj_data: A dict containing a node's basic data.
    :type obj_data: object
    """

    def __init__(self,obj_data):
        self.obj_data = obj_data

        self.name = obj_data["name"]
        self.class_name = obj_data["class"]
        self.description = obj_data["description"]
        self.command = obj_data["command"]
        self.x = obj_data["x"]
        self.y = obj_data["y"]
        self.uuid = obj_data["uuid"]
        self.out_nodes = obj_data["out_nodes"]
        self.in_node = obj_data["in_node"]
        self.error = False
        self.skip = False
        self.dirty = False
        self.state_label = None

        self._errors_list = list()
        self.messages = list()


    def setErrors(self,errors):
        self._errors_list = errors


    def getErrors(self):
        return self._errors_list
