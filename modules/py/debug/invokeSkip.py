from __future__ import division
from __future__ import generators
from __future__ import print_function
# from __future__ import absolute_import
#from __future__ import unicode_literals

import os, logging
from framework import resultObj


def main(data_block={}):
    """
    Invoking Skip so down-stream graph won't be calculated

    Return "skip"
    """

    _log = logging.getLogger(os.environ["ENSO_LOG"])
    _log.propagate = True
    _log.info(__name__)
    _log.warning("Simulating a skip...")

    skip_obj = resultObj.Spawn("skip")
    skip_obj.addMessage("This is an example skip node...")

    return skip_obj