from __future__ import print_function

print("Starting Haw-dini...")

import os
import sys
from framework import configs
from framework import editor
from framework import viewer
from framework.utilities import nodeUtils
from framework.app_py.main import Spawn as app_py
from PySide2.QtWidgets import QApplication


def setDefaults():
    """
    Initiates config and sets the defaults.
    These are all environment variables.

    **ENSO_PYVER** - *The system's Python version.*

    **ENSO_EDITOR_TITLE** - *The system's Python version.*

    **ENSO_VIEWER_TITLE** - *The system's Python version.*

    **ENSO_FRAMEWORK_ROOT** - *The framwork's root directory.*

    **ENSO_ROOT** - *Haw-dini's root directory.*

    **ENSO_MOD_PATH** - *Root directory for modules.*

    **ENSO_LOG_PATH** - *Root directory for logging.*

    **ENSO_DEFAULT_JSON** - *Full path of the default JSON graph.*

    **ENSO_DEFAULT_ICON** - *Full path of the default node icon.*

    **ENSO_ICONS_PATH** - *Root directory for the icons.*

    **ENSO_GLOBAL_CSS** - *Default CSS style for the UI.*

    **TEST_BLOCK** - *Default datablock for testing.*

    **STRICT** - *Strict mode.*

    **PROPAGATE** - *Propagate datablock information.*

    >>> enso.setDefaults()

    """

    configs.setup()


def setStrict(val="1"):
    """
    Sets strict mode.

    **parameters**, **types**, **return** and **return types**

    :param val: Sets strict mode on or off.
    :type val: bool

    >>> enso.setStrict(True)

    """

    if val not in ["0","1"]:
        raise ValueError('ENSO_STRICT should be string "0" or "1".')

    os.environ["ENSO_STRICT"] = str(val)
    print("Strict error checking set to:",val)


def setPropagate(val="1"):
    """
    Set the propagations switch for the datablock.

    **parameters**, **types**, **return** and **return types**

    :param val: Sets datablock propagation on or off.
    :type val: int

    >>> enso.setPropagate(True)

    """

    if val not in ["0","1"]:
        raise ValueError('ENSO_PROPAGATE should be string "0" or "1".')

    os.environ["ENSO_PROPAGATE"] = str(val)
    print("Datablock propagation set to:",val)


def setLanguage(lang_str="py"):
    """
    Use this to change the language to operate with beside Python.
    Also runs ``setup()`` once again to affect changes.

    **parameters**, **types**, **return** and **return types**

    :param lang_str: the language to be used. Valid: py, jsx, lua.
    :type lang_str: str

    >>> enso.setLanguage("jsx")

    """

    os.environ["ENSO_LANGUAGE"] = lang_str

    ENSO_MOD_PATH = os.path.join(os.environ["ENSO_ROOT"],
                               "modules",
                               os.environ["ENSO_LANGUAGE"])
    os.environ["ENSO_MOD_PATH"] = os.path.abspath(ENSO_MOD_PATH)


class Editor(editor.Spawn):
    """
    Runs the editor, used for authoring graphs.

    **parameters**, **types**, **return** and **return types**

    :param software: the platform to be ran. Example: maya, ae, max.
    :type software: str

    :param language: the language to be used. Valid: py, jsx, lua.
    :type language: str

    :param datablock: Serializable datablock (ideally).
    :type datablock: dict

    >>> enso.Editor("maya","py",datablock_dict)
    >>> enso.Editor("max","py",self.data)
    >>> enso.Editor("ae","jsx",datablock)
    >>> enso.Editor("fusion","lua",datablock)

    """

    def __init__(self,
                 software="generic",
                 language=os.environ["ENSO_LANGUAGE"],
                 graph_file = os.environ["ENSO_DEFAULT_JSON"],
                 datablock=configs.TEST_BLOCK):

        if language != os.environ["ENSO_LANGUAGE"]:
            setLanguage(language)

        _top_app = QApplication(sys.argv)
        super(Editor,self).__init__(software,
                                    language,
                                    graph_file,
                                    datablock)
        self.ui.show()
        print("Editor",software,language,graph_file)

        # done
        _top_app.exec_()
        sys.exit(0)


class Player(app_py):
    """
    Runs the player. Used for running graphs in Python env.
    Inherits app_py, calls viewer.

    **parameters**, **types**, **return** and **return types**

    :param json_file: Full path of JSON file.
    :type json_file: str

    :param datablock: Serializable datablock (ideally).
    :type datablock: dict

    >>> enso.Player(r"C:\tests\test_graph.json", datablock)
    >>> enso.Player(self.json_graph, self.datablock)

    """

    def __init__(self,
                 json_file=os.environ["ENSO_DEFAULT_JSON"],
                 datablock=configs.TEST_BLOCK):

        _top_app = QApplication(sys.argv)
        super(Player,self).__init__()

        self.strict = eval(os.environ["ENSO_STRICT"])
        self.propagate = eval(os.environ["ENSO_PROPAGATE"])
        _copy_block = datablock.copy()
        self.runJson(json_file,_copy_block)
        self.player = viewer.Spawn(json_file)
        self.player.log_btn.clicked.connect(self.openLog)

        for _n in self.nodes_all:
            _dummy_dict = {"name":_n.name,
                           "uuid":_n.uuid}

            _node = nodeUtils.getPointer(_dummy_dict,
                                          self.player.scene)
            _msg = "\n".join(_n.messages)
            _node.desc = _n.description
            _node.setErrors(_n.getErrors())

            if _n.error:
                _node.setDirty(state="error",msg=_msg)
            elif _n.skip:
                _node.setDirty(state="skip",msg=_msg)
            else:
                _node.setDirty()

        self.player.refresh()
        self.player.scene.setMode("player")
        self.player.feedback("Ran: {}".format(json_file))

        # done
        _top_app.exec_()
        sys.exit(0)


    def openLog(self):
        """
        Opens the log.txt file if self.log_file is a valid file in drive.
        """

        if os.path.isfile(self.log_file):
            if sys.platform in "win32":
                os.startfile(self.log_file)
            else:
                from subprocess import call as _sub_call
                _sub_call(["open",self.log_file])

            self.player.feedback("Opening log: {}".format(self.log_file))
        else:
            self.player.feedback("Log not found: {}".format(self.log_file))


class Viewer(app_py):
    """
    Runs the viewer.
    Inherits app_py, calls viewer.
    Used only for displaying results from non-python apps.

    **parameters**, **types**, **return** and **return types**

    :param graph_json: Full path of JSON graph that was just ran.
    :type graph_json: str

    :param score_json: Full path of JSON score extracted from graph_json.
    :type score_json: str

    >>> enso.Viewer(r"C:\graphs\ae_build.json", r"C:\score.json")

    """

    def __init__(self, graph_json, score_json):
        for j in (graph_json, score_json):
            if not os.path.exists(j):
                raise IOError("Not found:", j)

        _top_app = QApplication(sys.argv)
        super(Viewer,self).__init__()

        self.player = viewer.Spawn(graph_json)
        _score_data = self.getDataFromJson(score_json)

        for _score in _score_data:
            _node = nodeUtils.getPointer(_score, self.player.scene)

            if not _node:
                raise RuntimeError("Failed to find node pointer:",
                                   _score["name"],
                                   _score["uuid"])

            _msg = "\n".join(_score["messages"])
            _node.setErrors(_score.get("errors",list()))

            if _score["error"]:
                _node.setDirty(state="error",msg=_msg)
            elif _score["skip"]:
                _node.setDirty(state="skip",msg=_msg)
            else:
                _node.setDirty()

        # done
        _top_app.exec_()
        sys.exit(0)


if __name__ == "__main__":
    # Editor(language="jsx",graph_file="C:/repo/enso/graphs/tester_jsx_backup.json")
    # Editor()
    # Player()
    Viewer(r"C:/repo/enso/graphs/tester_jsx.json",r"C:\repo\enso\batman.json")
